import os
import re
import urllib.request
import codecs
import json
import sys
from os.path import join, exists
from django.utils.text import slugify

def urlretrieve(urlfile, fpath):
    print (urlfile.info()['Content-Length'] )
    chunk = 4096
    sum = 0
    f = open(fpath, "wb+")
    while 1:
        data = urlfile.read(chunk)
        if not data:
            print ("\ndone.\n")
            break
        f.write(data)
        sum += len(data) 
        print ('{} bytes'.format(sum), end='\r' )
        #sys.stdout.write('{0} imported\r'.format(tot))
        #sys.stdout.flush()

class TabletopWorkshop(object):
    def __init__(self):
        self.steam_workshop_downloader_url = 'http://steamworkshopdownloader.com/api/workshop/{}'
        self.reader = codecs.getreader('utf-8')
        self.cur_dir = os.path.dirname(os.path.abspath(__file__))
        self.userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'
        
    def get_module(self, module_id):
        url = self.steam_workshop_downloader_url.format(module_id)
        html = '' 
        with urllib.request.urlopen(url) as response:
            html = self.reader(response).read()            
        module_src = json.loads(html)
        '''
        print (module_src['file_size'])
        print (module_src['file_url'])
        print (module_src['preview_url'])
        print (module_src['title'])
        '''
        file_url = module_src['file_url']
        filename = 'workshopUpload' 
        dst_dir = join(self.cur_dir, slugify(module_src['title']) )
        print (dst_dir)
        if not exists(dst_dir):
            os.mkdir(dst_dir)       
        dst_file = join(dst_dir, filename)
        urllib.request.urlretrieve(file_url, dst_file)
        
        workshop_upload = open(dst_file, 'rb+')
        src = workshop_upload.read()
        bytes = bytearray(src)
        src = "".join(map(chr, bytes))
        #pos = src.find('http')
        links = img = re.compile(chr(0)+'http(.+?)'+chr(0), re.M|re.DOTALL).findall(src)
        #if len(links) == 0:
        #    links = img = re.compile(chr(30)+'http(.+?)'+chr(9)+'|'+chr(6)+'', re.M|re.DOTALL).findall(src)
        if len(links) == 0:
            links = img = re.compile('http(.+?) ', re.M|re.DOTALL).findall(src)
        new_links = []
        for link in links:
            new_links.append( 'http'+link )
        links = list(set(new_links))
        
        for link in links:
            try:
                print ( link )
                filename = link
                if filename.find(chr(10)) > -1:
                   continue
                
                if filename.endswith('{Unique}'):
                    filename = filename[:-8]
                    link = filename
                if filename.find('drive.google.com') > -1:
                    filename = 'googledrive_' + filename[filename.find('id=')+3:]
                if filename.find('pastebin.com/raw.php') > -1:
                    filename = 'pastebin_' + filename[filename.find('i=')+2:]
                else:
                    if link[-1] == '/':
                        filename = filename[:-1]
                    filename = filename[filename.rfind('/')+1:]
                                
                    if filename.rfind('?') > -1:
                        filename = filename[:filename.rfind('?')]
                
                        
                if exists( join(dst_dir, filename ) ):
                    file_nr = 1
                    while exists( join(dst_dir, filename + '_' + str(file_nr) ) ):
                        file_nr += 1
                    filename = join(dst_dir, filename + '_' + str(file_nr) )
                    
                print (filename)
                dst_file = join(dst_dir, filename )
                
                
                #req = urllib.request.Request(link, headers={'User-Agent': self.userAgent})
                
                request = urllib.request.Request(link)
                request.add_header('User-agent', self.userAgent)
                urlretrieve(urllib.request.urlopen(request), dst_file)
                #sys.exit()
                
                #urllib.request.urlretrieve(link, dst_file )
            except:
                print ( '!!! ERROR ERROR !!!' )
            
        
if __name__ == '__main__':
    tabletop = TabletopWorkshop()
    tabletop.get_module(260389428) # UNO