import imghdr
import os
from os import rename 


def set_ext(filename):
    new_ext = None
    hdr_ext = imghdr.what(filename)
    if hdr_ext == 'png':
        new_ext = 'png'
    elif hdr_ext == 'jpeg':
        new_ext = 'jpg'
        
    if new_ext != None:
        filename_new = filename + '.' + new_ext
        print filename_new
        rename(filename, filename_new)

if __name__ == '__main__':
    filenames = next(os.walk('.'))[2]
    for filename in filenames:
        if filename.find('.') == -1:
            set_ext(filename)